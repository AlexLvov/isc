/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------
-Preloader
*/



$(document).ready(function() {

  "use strict";

  // Скрипты из главной

   console.log('123');
   let Reviews = function () {
   this.slider = $('.reviews__slider');
   this.tabs = $('.reviews__link');
   this.sliderOptions = {
     lazyLoad: 'ondemand',
     dots: !0,
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: !0,
     arrows: !1,
     responsive: [
       {
         breakpoint: 1024,
         settings: {
           slidesToShow: 2,
           slidesToScroll: 2,
         }
       },
       {
         breakpoint: 587,
         settings: {
           slidesToShow: 1,
           slidesToScroll: 1
         }
       }
     ]
   };
   };
   Reviews.prototype.init = function(){
    this.makeItSlider();
    this.refreshSlider();
   };
   Reviews.prototype.makeItSlider = function () {
     let self = this;
     this.slider.slick(this.sliderOptions);
   };
   Reviews.prototype.refreshSlider = function(){
     let self = this;
     this.tabs.on('shown.bs.tab', function (e) {
       self.slider.slick('refresh');
     })
   };

   let Menu = function (){
     this.openButton = $('[data-target="mobile-menu"]');
     this.mobileNav = $('.modal-nav');
   };
   Menu.prototype.init = function(){
    this.setMobileMenuTrigger();
   };
   Menu.prototype.setMobileMenuTrigger = function(){
     let self = this;
     this.openButton.on('click', function () {
       self.mobileNav.modal('show');
     });
   };
   let MainProductSlider = function(){
   this.prodSlider = $('.main-product-slider');
   console.log(this.prodSlider);
   this.slickOptions = {
     dots: !0,
     slidesToShow: 1,
     slidesToScroll: 1,
     infinite: !0,
     arrows: !1,
     customPaging : function(slider, i) {
       let src = $(slider.$slides[i]).find('img').attr('src');
       return '<img src="' + src + '"/>';
     },
   };
   };
   MainProductSlider.prototype.init = function(){
     this.prodSlider.slick(this.slickOptions);
   };

   let MenuController = function(){
   this.menu = $('.nav-bar.scrolled');
   this.bottomNav = $('.advanced-links--mobile');
   };
   MenuController.prototype.init = function(){
     var self = this;
     $(document).on('scroll', function () {
       self.setScrollingControll();
     });
   };

   MenuController.prototype.setScrollingControll = function(){
     var self = this,
       st = $(window).scrollTop();
    if(st > 400){
      this.menu.show();
    }else{
      this.menu.hide();
    }
       if (st > self.lastScrollTop) {
         console.log('bottom');
         self.menu.removeClass('show');
         self.bottomNav.removeClass('show');

       } else {
         console.log('top');
         self.menu.addClass('show');
         self.bottomNav.addClass('show');
       }

     self.lastScrollTop = st;
   };

   let reviews = new Reviews();
   reviews.init();
   let menu = new Menu();
   menu.init();
   let mainProdSlider = new MainProductSlider();
   mainProdSlider.init();
   let menuController = new MenuController();
   menuController.init();



  var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
  ];

  let citiesInput = '.cities-input';
  let city_text = '.nav-bar__page--city';
  let modalNav = $('.modal-nav');
  $( citiesInput ).autocomplete({
    source: function(request, response) {
      var results = $.ui.autocomplete.filter(availableTags, request.term);

      response(results.slice(0, 2));
    }
  });
  $(document).on('scroll', function () {
    $(citiesInput).blur();
  });
  $(city_text).on('click', function () {
    let navBarCities = $(this).find('.nav-bar__cities');
    navBarCities.show();
    navBarCities.find(citiesInput).focus();
  });
  modalNav.on('blur', citiesInput, function () {
    modalNav.find('.nav-bar__cities').hide();
  });
  $( citiesInput ).on('autocompleteselect', function ( event, ui) {
    console.log('event ', event);

    console.log(123);
    $(this).blur();
    $(city_text).find('span').text(ui.item.value);
  });
  $( citiesInput ).on('blur', function () {
    $(this).parent().hide();
  });



  // Новые //

  // Preloader

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');


})
