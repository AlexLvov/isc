'head': {
    defaults: {
        title: 'Index',
        indexPage: true
    },
    team: {
        title: 'Команда',
        indexPage: false
    },
    home: {
        title: 'Home',
        indexPage: false
    }
}
,

__iconsData: {
    
        'advanced-rounds': {
            width: '320px',
            height: '214px'
        },
    
        'angle-right': {
            width: '256px',
            height: '512px'
        },
    
        'backet': {
            width: '18px',
            height: '17px'
        },
    
        'bag': {
            width: '21px',
            height: '19px'
        },
    
        'basket': {
            width: '18px',
            height: '17px'
        },
    
        'basket1': {
            width: '36px',
            height: '30px'
        },
    
        'call': {
            width: '34px',
            height: '34px'
        },
    
        'currier': {
            width: '46px',
            height: '45px'
        },
    
        'instagram': {
            width: '21px',
            height: '21px'
        },
    
        'instruction': {
            width: '16px',
            height: '10px'
        },
    
        'list-1': {
            width: '30px',
            height: '30px'
        },
    
        'list': {
            width: '53px',
            height: '67px'
        },
    
        'logo-footer': {
            width: '196px',
            height: '35px'
        },
    
        'logo': {
            width: '203px',
            height: '35px'
        },
    
        'magazine': {
            width: '42px',
            height: '42px'
        },
    
        'mail': {
            width: '26px',
            height: '16px'
        },
    
        'marker': {
            width: '10px',
            height: '14px'
        },
    
        'minus': {
            width: '13px',
            height: '3px'
        },
    
        'OK': {
            width: '14px',
            height: '25px'
        },
    
        'payment': {
            width: '44px',
            height: '43px'
        },
    
        'phone': {
            width: '20px',
            height: '19px'
        },
    
        'plus': {
            width: '13px',
            height: '14px'
        },
    
        'present': {
            width: '20px',
            height: '21px'
        },
    
        'schema': {
            width: '55px',
            height: '30px'
        },
    
        'state_iso': {
            width: '75px',
            height: '76px'
        },
    
        'state_register': {
            width: '53px',
            height: '67px'
        },
    
        'telegram': {
            width: '24px',
            height: '21px'
        },
    
        'telegram2': {
            width: '37px',
            height: '29px'
        },
    
        'user-card': {
            width: '50px',
            height: '44px'
        },
    
        'viber': {
            width: '32px',
            height: '30px'
        },
    
        'vk': {
            width: '19px',
            height: '12px'
        },
    
        'whats': {
            width: '31px',
            height: '29px'
        },
    
},

__pages: [{
                name: 'about',
                href: 'about.html'
             },{
                name: 'all',
                href: 'all.html'
             },{
                name: 'consultation',
                href: 'consultation.html'
             },{
                name: 'hangover',
                href: 'hangover.html'
             },{
                name: 'index',
                href: 'index.html'
             },{
                name: 'index000',
                href: 'index000.html'
             },{
                name: 'instruction',
                href: 'instruction.html'
             },{
                name: 'losing',
                href: 'losing.html'
             },{
                name: 'page',
                href: 'page.html'
             },{
                name: 'purification',
                href: 'purification.html'
             },{
                name: 'reviews',
                href: 'reviews.html'
             }]